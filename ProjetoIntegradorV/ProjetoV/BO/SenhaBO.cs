﻿
using ProjetoV.DAO;
using ProjetoV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjetoIntegradorV.BO
{
	public class SenhaBO
	{
		public string Gravar(Senha objSenha)
		{

			new SenhaDAO().Gravar(objSenha);
			return "Aluno Salvo com Sucesso!";
		}
		public IList<Senha> Listar()
		{
			return new SenhaDAO().Lista();
		}

		public Senha Buscar(int alunoID)
		{
			return new SenhaDAO().Buscar(alunoID);
		}
	}
}
