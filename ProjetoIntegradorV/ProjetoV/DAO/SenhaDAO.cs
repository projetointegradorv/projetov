﻿using ProjetoV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.Data;

namespace ProjetoV.DAO
{
	public class SenhaDAO
	{
		Database banco = Database.Open("dbBibliotecaWeb");

		public void Gravar(Senha objSenha)
		{
			if (objSenha.Id == 0)
			{
				var query = "Insert into Senha(id,senha,tipoAtendimento) Values(@0,@1,@2)";
				banco.Execute(query, objSenha.Id, objSenha.Password, objSenha.TipoAtendimento);
				banco.Close();
			}
			else
			{
				//var query = "Update Senha Set nome=@0," +
				//	"matricula=@1,dataNascimento=@2 Where alunoID=@3";
				//banco.Execute(query, objSenha.Nome, objSenha.Matricula,
				//	objSenha.DataNascimento, objSenha.AlunoID);
				//banco.Close();

			}

		}
		public IList<Senha> Lista()
		{
			IList<Senha> listaSenha = new List<Senha>();
			var query = "Select * From Senha";
			var resultado = banco.Query(query);
			if (resultado.Count() > 0)
			{
				Senha objSenha;
				foreach (var item in resultado)
				{
					objSenha = new Senha
					{
						Id = item.id,
						Password = item.password,
						TipoAtendimento = item.tipoAtendimento
					};
					listaSenha.Add(objSenha);
				}
				banco.Close();
			}
			else
			{
				banco.Close();
				return null;
			}
			return listaSenha;
		}
		public Senha Buscar(int senhaId)
		{
			var query = "Select * From Senha Where id = @0";
			var resultado = banco.QuerySingle(query, senhaId);
			Senha objSenha = new Senha
			{
				Id = resultado.id,
				Password = resultado.password,
				TipoAtendimento = resultado.tipoAtendimento
			};
			banco.Close();
			return objSenha;
		}
	}
}