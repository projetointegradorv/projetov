﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoV.Model
{
	public class Senha
	{
		public int Id { get; set; }
		public string Password { get; set; }
		public string TipoAtendimento { get; set; }
	}
}